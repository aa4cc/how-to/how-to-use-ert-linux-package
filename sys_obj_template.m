classdef sys_obj_qpOASES < matlab.System & coder.ExternalDependency ...
        % This Matlab object is responsible for:

    properties
        % Simulink !! does not !! allow parameters whose values are booleans, literal character
        % vectors, string objects or non-numeric structures to be tunable.

        % Do not put any fields here (maybe).
    end

    % Public, non-tunable properties
    properties(Nontunable)
        % Put parameters that will be set at the initialization here.
        % You can specify default values of the properties
        % E.g.:
        %   Port = '/dev/ttyUSB0';
    end

    % Properties that are allowed to change their values during simulation.
    % Use to store internal values
    properties(Access = private)
        % E.g.:
        %   state = 0;

    end

    methods
        % -----------------------------------
        % ---------Constructor---------------
        % -----------------------------------
        function obj = sys_obj_qpOASES(varargin)
            setProperties(obj,nargin,varargin{:}); % Set the properties based on the input in the 'mask'. Override the default parameters
        end
        % -----------------------------------
        % -----------------------------------

    end

    methods(Access = protected)

        % -----------------------------------
        % ---------Setup---------------------
        % -----------------------------------
        function setupImpl(obj)
            % This if/else is necessary in setupImpl when C-function is called
            if isempty(coder.target)
                % Just do nothing?
            else
                % Usually, you will call:
                %   "coder.cinclude('my_header.h')"
                % here and then
                %   "coder.ceval('initMyObj', args);
                % E.g.:
                %   coder.cinclude('SerialComERT_Wrapper.h');
                %   coder.ceval('initSerial', [obj.Port 0]);

            end
            % We could perform one-time calculations, such as computing constants
        end


        % -----------------------------------
        % ---------Step----------------------
        % -----------------------------------
        function [bytesReceived, header_f, SerialStatus] = stepImpl(obj)
            [bytesReceived, header_f]  = obj.catchMessage();
            SerialStatus = obj.SerialStatus;
        end


        % -------------------------------------------------------------
        % ---------Function to be called at the end of program---------
        % -------------------------------------------------------------
        function releaseImpl(obj)
            if isempty(coder.target)
                % Just do nothing?
            else
                % Call C-function implementing device termination
                % E.g.: 
                %   coder.ceval('close_ports');
            end
        end

        % ----------------------
        % Private functions 
        % ----------------------

        function [out1, out2] = foo(obj)
            out1 = 0;
            out2 = 0;
        end





        % ----------------------
        % INPUTS
        % ----------------------

        function num = getNumInputsImpl(~)
            num = 1;
        end

        % function in = getInputNamesImpl(obj)
        %     in = "Bytes In";
        % end

        function in = getInputDataTypeImpl(obj)
            in(1) = 'double';
        end

        function in = getInputSizeImpl(obj)
            in(1) = 1; % Size of init message is always 7 (TODO)
        end


        % ----------------------
        % OUTPUTS
        % ----------------------

        function num = getNumOutputsImpl(obj)
            num = 3;
        end

        function [bytesReceived, header_f, serialStatus] = getOutputSizeImpl(obj)
            bytesReceived = [1, obj.MessageByteLen];
            serialStatus = 1;
            header_f = 1;
        end

        % This function just needed to be here
        function [bytesReceived, header_f, serialStatus] = isOutputSizeLockedImpl(~,~)
            bytesReceived = true;
            serialStatus = true;
            header_f = true;
        end

        % ----------------------
        function [bytesReceived, header_f, serialStatus] = getOutputDataTypeImpl(obj)
            bytesReceived = 'uint8';
            serialStatus = 'int8';
            header_f = 'int8';
        end

        % ----------------------
        function [bytesReceived, header_f, serialStatus] = isOutputFixedSizeImpl(obj,~)
            bytesReceived = true;
            serialStatus = true;
            header_f = true;
        end

        % ----------------------
        function [bytesReceived, header_f, serialStatus] = isOutputComplexImpl(obj)
            bytesReceived = false;
            serialStatus = false;
            header_f = false;
        end

        % TODO: This function appears to not working
        %         function out = getOutputNamesImpl(obj)
        %             out(1) = "Bytes Out";
        %             out(2) = "Serial status";
        %             out(3) = "Header flag";
        %         end


        function icon = getIconImpl(obj)
            icon = ["Serial", "Read"]; % Example: text icon
            % icon = ["My","System"]; % Example: multi-line text icon
            % icon = matlab.system.display.Icon("myicon.jpg"); % Example: image file icon
        end

    end




    methods (Static, Access=protected)
        function simMode = getSimulateUsingImpl(~)
            simMode = 'Interpreted execution';
        end

        function isVisible = showSimulateUsingImpl
            isVisible = false;
        end
    end



    methods(Static)
        function name = getDescriptiveName()
            name = 'SMT';
        end

        function b = isSupportedContext(context)
            b = context.isCodeGenTarget('rtw');
        end

        function updateBuildInfo(buildInfo, context)
            % IMPORTANT
            if context.isCodeGenTarget('rtw')
%                 srcDir = fullfile(fileparts(mfilename('fullpath')),'Cpp');
%                 includeDir = fullfile(fileparts(mfilename('fullpath')),'Cpp');
%                 addIncludePaths(buildInfo,includeDir);
% 
%                 addIncludeFiles(buildInfo,'SerialStream.h', includeDir);
%                 addIncludeFiles(buildInfo,'SerialComERT_Wrapper.h', includeDir);
%                 addIncludeFiles(buildInfo,'SerialPortConstants.h', includeDir);
%                 addIncludeFiles(buildInfo,'SerialStreamBuf.h', includeDir);
%                 addIncludeFiles(buildInfo,'SerialPort.h', includeDir);
% 
%                 addSourceFiles(buildInfo,'SerialComERT.cpp', srcDir);
%                 addSourceFiles(buildInfo,'SerialStream.cpp', srcDir);
%                 addSourceFiles(buildInfo,'SerialStreamBuf.cpp', srcDir);
%                 addSourceFiles(buildInfo,'SerialPort.cpp', srcDir);

                addCompileFlags(buildInfo,{'-std=c++14'});
            end
        end

    end
end