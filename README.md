How to use ERT Linux Package
==============

This guide shows, how to use the ERT Linux Package that generates a *C* code from a Simulink Block Diagram and then runs the code on a Linux target (e.g. Rappberry Pi).

# Preliminaries

## Linux machine

### Matlab/Simulink
- Linux machine that is able to run the Matlab/Simulink with packages:
    - TODO

### Xterm    
- Installed **xterm**:
    
        > sudo apt-get install -y xterm

### Cross-compiler
- Cross-compiler from the Linux machine to the target machine
    - You can find the cross-compiler for the Linux machine to the target here: 
        <https://github.com/abhiTronix/raspberry-pi-cross-compilers>.
		    
        - Linux machine: **any x64/x86 Linux machine**
        - Target: on a RPi, you can find the correct compiler by running a command in the terminal:
            
                cat /etc/*-release

        -  Most probably, the cross-compiler will be: 
        <https://github.com/abhiTronix/raspberry-pi-cross-compilers/wiki/Cross-Compiler:-Installation-Instructions>.
		- Recommended is **Permanent Installation**
		- Check, that the compiler is in the path by typing the name of the compilers in the terminal.
			- Probably, the name of the compilers will be:
				- C: **aarch64-linux-gnu-gcc**
				- C++: **aarch64-linux-gnu-g++**

### Passwordless SSH
- The purpose of this is to send the compiled code to the target without any obstacles
- For instance, follow the instruction at <https://phoenixnap.com/kb/setup-passwordless-ssh> or google it elsewhere.
	- Note, there is a small mistake in the command: 
	        
            copy-id [remote_username]@[server_ip_address]
	  
# Instructions
- Clone (or download) the repository: <https://github.com/aa4cc/ert_linux> and follow the **Installation** instructions. Thus, in Matlab prompt, execute:

    	cd <root>/ert_linux/ert_linux
    	ert_linux_setup

- Open **./ert_linux/ert_linux/ert_linux_run_model.m** and replace line 24:
	```
	system(sprintf('xterm -e "scp %s ./%s %s@%s:/tmp ; echo model %s copied to target is run ; ssh %s %s@%s /tmp/%s -tf inf -w ; sleep 2" &', ssh_key_opt, model_app, target_user, target_ip_addr, model_app, ssh_key_opt, target_user, target_ip_addr, model_app));
	```
	by
	```
	system(sprintf('xterm -e "scp %s ./%s %s@%s:/tmp ; echo model %s copied to target is run ; ssh %s %s@%s sudo /tmp/%s -tf inf -w ; sleep 2" &', ssh_key_opt, model_app, target_user, target_ip_addr, model_app, ssh_key_opt, target_user, target_ip_addr, model_app));
	```

    This allows to run the code on Target with **sudo** permission so, for instance, HW port can be opened.

- Open **./ert_linux/model_samples/model_sample.slx**
	- In the highlighted box: **Run on the remote target (aarch64)**
		- Put the correct compilers into the block with **make_rtw...** command.

- Put the correct **IP address** and **username** of the target into the box.
- Double-click on **Build**, double-click on **Deploy and connect**.
- New pane **HARDWARE** should appear.
- Press **Run (TODO: or Monitor & Tune?)**.


# How to link external libraries

In a current version of ERT_linux (branch master, commit 7dc750dacd22cfc89426b4760defa2c21fe98c79), there is probably no nice way to do it. The workaround is:

- Compile the library for the target device (i.e. compile the library using the RPi's compiler). This might be done by changing Makefile. The static library (extension '.a') should be ok.

- In the file **./ert_linux/ert_linux.tmf** , line 213 : change the variable "SYSTEM_LIBS +=" to include your library using arguments -l... and -L... . For example, to add the library file: **libqpOASES_wrapper.a** located in **/home/loido/libraryDIR** :

	- SYSTEM_LIBS += -lm -lrt -lpthread -lqpOASES_wrapper -L/home/loido/libraryDIR
	
- In system object m-file, include the header file needed to call the library. E.g.:
	
	- includeDir = fullfile(fileparts(mfilename('fullpath')),'qpoases_ERT');
	- addIncludePaths(buildInfo,includeDir);
	- addIncludeFiles(buildInfo,'qpOASES_wrapper.h', includeDir);
	

	